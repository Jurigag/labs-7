﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7.ViewModel
{
    public class ViewModel_1 : ViewModel
    {
        private ImageSource imageUrl;
        public ImageSource ImageUrl
        {
            get
            {
                return imageUrl;
            }

            set
            {
                imageUrl = value;
                FirePropertyChanged("ImageUrl");
            }
        }
    }
}
