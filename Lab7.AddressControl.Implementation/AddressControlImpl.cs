﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    public class AddressControlImpl:IAddress
    {
        private Control window;
        private Command command;
        private string url;


        public static bool DesignMode
        {
            get { return DesignerProperties.GetIsInDesignMode(new DependencyObject()); }
        }

        public AddressControlImpl()
       {
           if (!DesignMode)
           {
               this.window = new AddressControl();
               this.Command = new Command(FireAdressChanged);
               this.window.DataContext = this;
           }
       }

        private void FireAdressChanged(object obj)
        {
            EventHandler<AddressChangedArgs> handler = (obj as AddressControlImpl).AddressChanged;
            if (handler != null)
            {
                AddressChangedArgs arg = new AddressChangedArgs();
                arg.URL = url;
                AddressChanged(this, arg);
            }
        }

        Control IAddress.Control
        {
            get { return window; }
        }
        public event EventHandler<AddressChangedArgs> AddressChanged;

        public Command Command
        {
            get { return command; }
            set { command = value; }
        }
        public string Url
        {
            get { return url; }
            set { url = value; this.Command.Execute(this); }
        }
    }
}
