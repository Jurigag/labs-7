﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImageImpl:IRemoteImage
    {
        private int firstTime = 0;
        private class ViewModel_1:ViewModel
        {
            private ImageSource imageUrl;
            public ImageSource ImageUrl
            {
                get
                {
                    return imageUrl;
                }

                set
                {
                    imageUrl = value;
                    FirePropertyChanged("ImageUrl");
                }
            }
        }

        ViewModel_1 vm;

        public Control window;

        Control IRemoteImage.Control
        {
            get
            {
                return window;
            }
        }

        public bool CheckURLValid(string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }

        public void Load(string url)
        {
            if (CheckURLValid(url))
            {
                StartLoading += OnBeginLoading;
                EndLoading += OnEndLoading;
                StartLoading(this, new EventArgs());
                WebClient web = new WebClient();
                web.Proxy = null;
                MemoryStream ms = new MemoryStream(web.DownloadData(new Uri(url)));
                ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
                vm.ImageUrl = (ImageSource)imageSourceConverter.ConvertFrom(ms);
                EndLoading(this, new EventArgs());
            }
        }

        async void LoadAsync(string url)
        {
            if (CheckURLValid(url))
            {
                await Task.Run(() => StartLoading(this, new EventArgs()));
                WebClient client = new WebClient();
                client.Proxy = null;
                client.DownloadDataCompleted += DownloadDataCompleted;
                client.DownloadDataAsync(new Uri(url));
            }
        }

        private IAddress url;
        public RemoteImageImpl(IAddress u)
        {
            window = new RemoteImage();
            url = u;
            vm = new ViewModel_1();
            url.AddressChanged += new System.EventHandler<AddressChangedArgs>(this.OnAddressChanged);
            window.DataContext = vm;
        }

        public void OnAddressChanged(object sender, AddressChangedArgs args)
        {
            if (firstTime == 0)
            {
                Load(args.URL);
                firstTime++;
            }
            else
            {
                LoadAsync(args.URL);
            }
        }

        public event EventHandler<EventArgs> StartLoading;
        public event EventHandler<EventArgs> EndLoading;

        private void OnEndLoading(object sender, EventArgs e)
        {
            MessageBox.Show("End");
        }

        private void OnBeginLoading(object sender, EventArgs e)
        {
            MessageBox.Show("Begin");
        }

        private void DownloadDataCompleted(object sender,
    DownloadDataCompletedEventArgs e)
        {
            MemoryStream ms = new MemoryStream(e.Result);
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            vm.ImageUrl = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            EndLoading(this, new EventArgs());
        }
    }
}
