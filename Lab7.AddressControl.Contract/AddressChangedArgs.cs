﻿using System;

namespace Lab7.AddressControl.Contract
{

    public class AddressChangedArgs : EventArgs
    {
        public string URL;
    }
}
